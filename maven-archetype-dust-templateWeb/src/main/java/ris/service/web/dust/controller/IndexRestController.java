package ris.service.web.dust.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is a sample {@link RestController} that will map HTTP requests to given methods with 
 * matching @RequestMapping URIs.  In this case, all requests that start with "/sample" will be 
 * mapped to this controller, and further URI node mappings will be specified at the method 
 * level (see {@link #retrieveSample(String, String)}).
 */
@Transactional
@Controller
public class IndexRestController {	
	
	private static final Log LOG = LogFactory.getLog(IndexRestController.class);
	
	@RequestMapping(value="/index",
			method=RequestMethod.GET,
			produces={MediaType.APPLICATION_JSON_VALUE})
	public SampleResource retrieveSample() throws Exception {
		LOG.debug("Request start");
		SampleResource sample = new SampleResource();
		sample.setContract("123456");
		sample.setMemberId("987456321");
		return sample;
	}
}