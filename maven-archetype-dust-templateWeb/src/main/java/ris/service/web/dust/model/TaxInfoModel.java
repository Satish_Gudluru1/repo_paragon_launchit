package ris.service.web.dust.model;

import java.math.BigDecimal;

import com.principal.ris.annuity.editable.tax.domain.DistributionCode;
import com.principal.ris.annuity.editable.tax.domain.StateWithholdingElection;

/**
 * This is a model object for Tax information.
 * @author Satish Gudluru
 *
 */
public class TaxInfoModel extends BaseModel {
	

	private boolean taxIdCertification;
	
	private String taxId;
	
	private boolean backupFedWithholding ;
	
	private boolean ownerTaxidCertification; //FlexibleWithdrawalOptionRequest.ownerTaxidCertification
	
	private boolean nonResidentAlienW8BENReceiptIndicator; //FlexibleWithdrawalOption.requestAuthorization.nonResidentAlienW8BENReceiptIndicator
	
	private BigDecimal specifiedFedPercentage ;//FlexibleWithdrawalOptionRequest.taxEditable.specifiedFedPercentage
	
	private StateWithholdingElection stateWithholdingElection; //FlexibleWithdrawalOptionRequest.taxEditable.stateWithholdingElection
	
	private BigDecimal specifiedStatePercentage; // FlexibleWithdrawalOptionRequest.taxEditable.specifiedStatePercentage
	
	private Boolean tax1099RDistributionCodeChangeIndicator; //FlexibleWithdrawalOptionRequest.requestAuthorization.tax1099RDistributionCodeChangeIndicator
	
	private DistributionCode distributionCode; //FlexibleWithdrawalOptionRequest.taxEditable.distributionCode	
	
	// //List<ContractRole> -> If(ContractRole.roleType == OWNER && ContractRole.primaryOwner == true) THEN USE ContractRole.party.taxType
	private String taxType; 
	/* List<ContractRole> -> if(ContractRole.roleType == OWNER && ContractRole.primaryOwner == true && ContractRole.party instanceof Person) 
	 THEN USE ContractRole.person.citizenshipCountryCode */
	private int citizenshipCountryCode ;
	
	private int taxStateCode;
	
	
	public boolean isTaxIdCertification() {
		return taxIdCertification;
	}
	
	public void setTaxIdCertification(boolean taxIdCertification) {
		this.taxIdCertification = taxIdCertification;
	}
	
	public String getTaxId() {
		return taxId;
	}
	
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	
	public boolean isBackupFedWithholding() {
		return backupFedWithholding;
	}
	
	public void setBackupFedWithholding(boolean backupFedWithholding) {
		this.backupFedWithholding = backupFedWithholding;
	}
	
	public boolean isOwnerTaxidCertification() {
		return ownerTaxidCertification;
	}
	
	public void setOwnerTaxidCertification(boolean ownerTaxidCertification) {
		this.ownerTaxidCertification = ownerTaxidCertification;
	}
	
	public boolean isNonResidentAlienW8BENReceiptIndicator() {
		return nonResidentAlienW8BENReceiptIndicator;
	}
	
	public void setNonResidentAlienW8BENReceiptIndicator(boolean nonResidentAlienW8BENReceiptIndicator) {
		this.nonResidentAlienW8BENReceiptIndicator = nonResidentAlienW8BENReceiptIndicator;
	}
	
	public BigDecimal getSpecifiedFedPercentage() {
		return specifiedFedPercentage;
	}
	
	public void setSpecifiedFedPercentage(BigDecimal specifiedFedPercentage) {
		this.specifiedFedPercentage = specifiedFedPercentage;
	}
	
	public StateWithholdingElection getStateWithholdingElection() {
		return stateWithholdingElection;
	}
	
	public void setStateWithholdingElection(StateWithholdingElection stateWithholdingElection) {
		this.stateWithholdingElection = stateWithholdingElection;
	}
	
	public BigDecimal getSpecifiedStatePercentage() {
		return specifiedStatePercentage;
	}
	public void setSpecifiedStatePercentage(BigDecimal specifiedStatePercentage) {
		this.specifiedStatePercentage = specifiedStatePercentage;
	}
	
	public Boolean getTax1099RDistributionCodeChangeIndicator() {
		return tax1099RDistributionCodeChangeIndicator;
	}
	
	public void setTax1099RDistributionCodeChangeIndicator(Boolean tax1099rDistributionCodeChangeIndicator) {
		tax1099RDistributionCodeChangeIndicator = tax1099rDistributionCodeChangeIndicator;
	}
	
	public DistributionCode getDistributionCode() {
		return distributionCode;
	}
	
	public void setDistributionCode(DistributionCode distributionCode) {
		this.distributionCode = distributionCode;
	}
	
	public String getTaxType() {
		return taxType;
	}
	
	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}
	
	public int getCitizenshipCountryCode() {
		return citizenshipCountryCode;
	}
	
	public void setCitizenshipCountryCode(int citizenshipCountryCode) {
		this.citizenshipCountryCode = citizenshipCountryCode;
	}
	
	public int getTaxStateCode() {
		return taxStateCode;
	}
	
	public void setTaxStateCode(int taxStateCode) {
		this.taxStateCode = taxStateCode;
	}	
	

}
