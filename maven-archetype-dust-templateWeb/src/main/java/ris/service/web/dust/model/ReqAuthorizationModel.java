package ris.service.web.dust.model;

import java.util.Date;

import com.principal.ris.annuity.request.authorization.domain.AuthorizationCode;
import com.principal.ris.annuity.request.authorization.domain.AuthorizationRelationshipCode;
import com.principal.ris.annuity.request.authorization.domain.SubmissionRequest;

/**
 * This is a model object for Request authorization information.
 * @author Satish Gudluru
 *
 */
public class ReqAuthorizationModel extends BaseModel {
	
	//	List<ContractRole> -> 	If(ContractRole.roleType == OWNER && ContractRole.primaryOwner == true) THEN 	ConractRole.party.taxType
	private int taxType; 

	//FlexibleWithdrawalOptionRequest.requestAuthorization.customerRequestSubmissionDocType
	private SubmissionRequest customerRequestSubmissionDocType;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.assignmentIndicator
	private boolean assignmentIndicator;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.signeeTrustOwnerRelationshipCode
	private boolean signeeTrustOwnerRelationshipCode;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.trustSigneeSignatureConsistencyIndicator
	private boolean trustSigneeSignatureConsistencyIndicator;
	
	 //FlexibleWithdrawalOptionRequest.requestAuthorization.orgBoardOfResolutionDocReceiptIndicator
	private boolean orgBoardOfResolutionDocReceiptIndicator;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.signeeOrgOwnerRelationshipIndicator
	private boolean signeeOrgOwnerRelationshipIndicator;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.orgSigneeSignatureConsistencyIndicator
	private boolean orgSigneeSignatureConsistencyIndicator;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.signeeOwnerRelationshipCode
	private AuthorizationRelationshipCode signeeOwnerRelationshipCode;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.ownerPowerOfAttorneyAffidavitDocReceiptIndicator
	private boolean ownerPowerOfAttorneyAffidavitDocReceiptIndicator;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.ownerPowerOfAttorneyLegalReviewReqStatusCode
	private boolean ownerPowerOfAttorneyLegalReviewReqStatusCode;
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.ownerPowerOfAttorneyLegalReviewCode
	private AuthorizationCode ownerPowerOfAttorneyLegalReviewCode;
	
	//"FlexibleWithdrawalOptionRequest.requestAuthorization.ownerAttorneyInFactSigneeSignatureConsistencyIndicator"
	private boolean ownerAttorneyInFactSigneeSignatureConsistencyIndicator;
	
	//"FlexibleWithdrawalOptionRequest.requestAuthorization.ownerPowerOfAttorneyAffidavitDocSignatureDate"
	private Date ownerPowerOfAttorneyAffidavitDocSignatureDate;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.ownerSigneeDocReceiptIndicator"
	private boolean ownerSigneeDocReceiptIndicator;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.ownerSigneeLegalReviewReqStatusCode"
	private boolean ownerSigneeLegalReviewReqStatusCode;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.ownerSigneeLegalReviewCode"
	private AuthorizationCode ownerSigneeLegalReviewCode;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.ownerSigneeSignatureConsistencyIndicator"
	private boolean ownerSigneeSignatureConsistencyIndicator;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.ownerSelfSigneeSignatureConsistencyIndicator"
	private boolean ownerSelfSigneeSignatureConsistencyIndicator;
	
	//"FlexibleWithdrawalOptionRequest.requestAuthorization.jointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator"
	private boolean jointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator;
	
	 //"FlexibleWithdrawalOptionRequest.	requestAuthorization.jointOwnerPowerOfAttorneyLegalReviewReqStatusCode"
	private boolean jointOwnerPowerOfAttorneyLegalReviewReqStatusCode;
	
	//"FlexibleWithdrawalOptionRequest.	requestAuthorization.	jointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator"
	private boolean jointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator;
	
	//FlexibleWithdrawalOptionRequest.	requestAuthorization.	jointOwnerPowerOfAttorneyAffidavitDocSignatureDate
	private Date jointOwnerPowerOfAttorneyAffidavitDocSignatureDate;
	
	//"FlexibleWithdrawalOptionRequest.	requestAuthorization.	jointOwnerSigneeDocReceiptIndicator"
	private boolean jointOwnerSigneeDocReceiptIndicator;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.jointOwnerSigneeLegalReviewReqStatusCode"
	private boolean jointOwnerSigneeLegalReviewReqStatusCode;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.jointOwnerSigneeAuthorizationCode"
	private AuthorizationCode jointOwnerSigneeAuthorizationCode;

	//"FlexibleWithdrawalOptionRequest.requestAuthorization.jointOwnerSigneeSignatureConsistencyIndicator"
	private boolean jointOwnerSigneeSignatureConsistencyIndicator;
	
	//"FlexibleWithdrawalOptionRequest.requestAuthorization.jointOwnerSelfSigneeSignatureConsistencyIndicator"
	private boolean jointOwnerSelfSigneeSignatureConsistencyIndicator;
	
	public int getTaxType() {
		return taxType;
	}

	public void setTaxType(int taxType) {
		this.taxType = taxType;
	}

	public SubmissionRequest getCustomerRequestSubmissionDocType() {
		return customerRequestSubmissionDocType;
	}

	public void setCustomerRequestSubmissionDocType(SubmissionRequest customerRequestSubmissionDocType) {
		this.customerRequestSubmissionDocType = customerRequestSubmissionDocType;
	}

	public boolean isAssignmentIndicator() {
		return assignmentIndicator;
	}

	public void setAssignmentIndicator(boolean assignmentIndicator) {
		this.assignmentIndicator = assignmentIndicator;
	}

	public boolean isSigneeTrustOwnerRelationshipCode() {
		return signeeTrustOwnerRelationshipCode;
	}

	public void setSigneeTrustOwnerRelationshipCode(boolean signeeTrustOwnerRelationshipCode) {
		this.signeeTrustOwnerRelationshipCode = signeeTrustOwnerRelationshipCode;
	}

	public boolean isTrustSigneeSignatureConsistencyIndicator() {
		return trustSigneeSignatureConsistencyIndicator;
	}

	public void setTrustSigneeSignatureConsistencyIndicator(boolean trustSigneeSignatureConsistencyIndicator) {
		this.trustSigneeSignatureConsistencyIndicator = trustSigneeSignatureConsistencyIndicator;
	}

	public boolean isOrgBoardOfResolutionDocReceiptIndicator() {
		return orgBoardOfResolutionDocReceiptIndicator;
	}

	public void setOrgBoardOfResolutionDocReceiptIndicator(boolean orgBoardOfResolutionDocReceiptIndicator) {
		this.orgBoardOfResolutionDocReceiptIndicator = orgBoardOfResolutionDocReceiptIndicator;
	}

	public boolean isSigneeOrgOwnerRelationshipIndicator() {
		return signeeOrgOwnerRelationshipIndicator;
	}

	public void setSigneeOrgOwnerRelationshipIndicator(boolean signeeOrgOwnerRelationshipIndicator) {
		this.signeeOrgOwnerRelationshipIndicator = signeeOrgOwnerRelationshipIndicator;
	}

	public boolean isOrgSigneeSignatureConsistencyIndicator() {
		return orgSigneeSignatureConsistencyIndicator;
	}

	public void setOrgSigneeSignatureConsistencyIndicator(boolean orgSigneeSignatureConsistencyIndicator) {
		this.orgSigneeSignatureConsistencyIndicator = orgSigneeSignatureConsistencyIndicator;
	}

	public AuthorizationRelationshipCode getSigneeOwnerRelationshipCode() {
		return signeeOwnerRelationshipCode;
	}

	public void setSigneeOwnerRelationshipCode(AuthorizationRelationshipCode signeeOwnerRelationshipCode) {
		this.signeeOwnerRelationshipCode = signeeOwnerRelationshipCode;
	}

	public boolean isOwnerPowerOfAttorneyAffidavitDocReceiptIndicator() {
		return ownerPowerOfAttorneyAffidavitDocReceiptIndicator;
	}

	public void setOwnerPowerOfAttorneyAffidavitDocReceiptIndicator(
			boolean ownerPowerOfAttorneyAffidavitDocReceiptIndicator) {
		this.ownerPowerOfAttorneyAffidavitDocReceiptIndicator = ownerPowerOfAttorneyAffidavitDocReceiptIndicator;
	}

	public boolean isOwnerPowerOfAttorneyLegalReviewReqStatusCode() {
		return ownerPowerOfAttorneyLegalReviewReqStatusCode;
	}

	public void setOwnerPowerOfAttorneyLegalReviewReqStatusCode(boolean ownerPowerOfAttorneyLegalReviewReqStatusCode) {
		this.ownerPowerOfAttorneyLegalReviewReqStatusCode = ownerPowerOfAttorneyLegalReviewReqStatusCode;
	}

	public AuthorizationCode getOwnerPowerOfAttorneyLegalReviewCode() {
		return ownerPowerOfAttorneyLegalReviewCode;
	}

	public void setOwnerPowerOfAttorneyLegalReviewCode(AuthorizationCode ownerPowerOfAttorneyLegalReviewCode) {
		this.ownerPowerOfAttorneyLegalReviewCode = ownerPowerOfAttorneyLegalReviewCode;
	}

	public boolean isOwnerAttorneyInFactSigneeSignatureConsistencyIndicator() {
		return ownerAttorneyInFactSigneeSignatureConsistencyIndicator;
	}

	public void setOwnerAttorneyInFactSigneeSignatureConsistencyIndicator(
			boolean ownerAttorneyInFactSigneeSignatureConsistencyIndicator) {
		this.ownerAttorneyInFactSigneeSignatureConsistencyIndicator = ownerAttorneyInFactSigneeSignatureConsistencyIndicator;
	}

	public Date getOwnerPowerOfAttorneyAffidavitDocSignatureDate() {
		return ownerPowerOfAttorneyAffidavitDocSignatureDate;
	}

	public void setOwnerPowerOfAttorneyAffidavitDocSignatureDate(Date ownerPowerOfAttorneyAffidavitDocSignatureDate) {
		this.ownerPowerOfAttorneyAffidavitDocSignatureDate = ownerPowerOfAttorneyAffidavitDocSignatureDate;
	}

	public boolean isOwnerSigneeDocReceiptIndicator() {
		return ownerSigneeDocReceiptIndicator;
	}

	public void setOwnerSigneeDocReceiptIndicator(boolean ownerSigneeDocReceiptIndicator) {
		this.ownerSigneeDocReceiptIndicator = ownerSigneeDocReceiptIndicator;
	}

	public boolean isOwnerSigneeLegalReviewReqStatusCode() {
		return ownerSigneeLegalReviewReqStatusCode;
	}

	public void setOwnerSigneeLegalReviewReqStatusCode(boolean ownerSigneeLegalReviewReqStatusCode) {
		this.ownerSigneeLegalReviewReqStatusCode = ownerSigneeLegalReviewReqStatusCode;
	}

	public AuthorizationCode getOwnerSigneeLegalReviewCode() {
		return ownerSigneeLegalReviewCode;
	}

	public void setOwnerSigneeLegalReviewCode(AuthorizationCode ownerSigneeLegalReviewCode) {
		this.ownerSigneeLegalReviewCode = ownerSigneeLegalReviewCode;
	}

	public boolean isOwnerSigneeSignatureConsistencyIndicator() {
		return ownerSigneeSignatureConsistencyIndicator;
	}

	public void setOwnerSigneeSignatureConsistencyIndicator(boolean ownerSigneeSignatureConsistencyIndicator) {
		this.ownerSigneeSignatureConsistencyIndicator = ownerSigneeSignatureConsistencyIndicator;
	}

	public boolean isOwnerSelfSigneeSignatureConsistencyIndicator() {
		return ownerSelfSigneeSignatureConsistencyIndicator;
	}

	public void setOwnerSelfSigneeSignatureConsistencyIndicator(boolean ownerSelfSigneeSignatureConsistencyIndicator) {
		this.ownerSelfSigneeSignatureConsistencyIndicator = ownerSelfSigneeSignatureConsistencyIndicator;
	}

	public boolean isJointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator() {
		return jointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator;
	}

	public void setJointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator(
			boolean jointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator) {
		this.jointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator = jointOwnerPowerOfAttorneyAffidavitDocReceiptIndicator;
	}

	public boolean isJointOwnerPowerOfAttorneyLegalReviewReqStatusCode() {
		return jointOwnerPowerOfAttorneyLegalReviewReqStatusCode;
	}

	public void setJointOwnerPowerOfAttorneyLegalReviewReqStatusCode(
			boolean jointOwnerPowerOfAttorneyLegalReviewReqStatusCode) {
		this.jointOwnerPowerOfAttorneyLegalReviewReqStatusCode = jointOwnerPowerOfAttorneyLegalReviewReqStatusCode;
	}

	public boolean isJointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator() {
		return jointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator;
	}

	public void setJointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator(
			boolean jointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator) {
		this.jointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator = jointOwnerAttorneyInFactSigneeSignatureConsistencyIndicator;
	}

	public Date getJointOwnerPowerOfAttorneyAffidavitDocSignatureDate() {
		return jointOwnerPowerOfAttorneyAffidavitDocSignatureDate;
	}

	public void setJointOwnerPowerOfAttorneyAffidavitDocSignatureDate(
			Date jointOwnerPowerOfAttorneyAffidavitDocSignatureDate) {
		this.jointOwnerPowerOfAttorneyAffidavitDocSignatureDate = jointOwnerPowerOfAttorneyAffidavitDocSignatureDate;
	}

	public boolean isJointOwnerSigneeDocReceiptIndicator() {
		return jointOwnerSigneeDocReceiptIndicator;
	}

	public void setJointOwnerSigneeDocReceiptIndicator(boolean jointOwnerSigneeDocReceiptIndicator) {
		this.jointOwnerSigneeDocReceiptIndicator = jointOwnerSigneeDocReceiptIndicator;
	}

	public boolean isJointOwnerSigneeLegalReviewReqStatusCode() {
		return jointOwnerSigneeLegalReviewReqStatusCode;
	}

	public void setJointOwnerSigneeLegalReviewReqStatusCode(boolean jointOwnerSigneeLegalReviewReqStatusCode) {
		this.jointOwnerSigneeLegalReviewReqStatusCode = jointOwnerSigneeLegalReviewReqStatusCode;
	}

	public AuthorizationCode getJointOwnerSigneeAuthorizationCode() {
		return jointOwnerSigneeAuthorizationCode;
	}

	public void setJointOwnerSigneeAuthorizationCode(AuthorizationCode jointOwnerSigneeAuthorizationCode) {
		this.jointOwnerSigneeAuthorizationCode = jointOwnerSigneeAuthorizationCode;
	}

	public boolean isJointOwnerSigneeSignatureConsistencyIndicator() {
		return jointOwnerSigneeSignatureConsistencyIndicator;
	}

	public void setJointOwnerSigneeSignatureConsistencyIndicator(boolean jointOwnerSigneeSignatureConsistencyIndicator) {
		this.jointOwnerSigneeSignatureConsistencyIndicator = jointOwnerSigneeSignatureConsistencyIndicator;
	}

	public boolean isJointOwnerSelfSigneeSignatureConsistencyIndicator() {
		return jointOwnerSelfSigneeSignatureConsistencyIndicator;
	}

	public void setJointOwnerSelfSigneeSignatureConsistencyIndicator(
			boolean jointOwnerSelfSigneeSignatureConsistencyIndicator) {
		this.jointOwnerSelfSigneeSignatureConsistencyIndicator = jointOwnerSelfSigneeSignatureConsistencyIndicator;
	}

	
}
