package ris.service.web.dust.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class pfgUtils {
	
	
public static List<String> xmlRender(String what,String tFileLocation){
		
		
		List<String> sectionList = new ArrayList<String>(); 
		 DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
		 try {
				
			 DocumentBuilder builder = factory.newDocumentBuilder();
			 Document document = builder.parse(new File(tFileLocation+"PFGXML.xml"));
		      document.getDocumentElement().normalize();
		      NodeList nList = document.getElementsByTagName(what);
		     
		      for(int j = 0; j<nList.getLength();j++){
		    	  
		    	  Node node = nList.item(j);
		    	  NodeList childNodes = node.getChildNodes();
		    	
		    	  for(int k=0;k<childNodes.getLength();k++){
		    		  Node cNode = childNodes.item(k);
		    		  if (cNode instanceof Element) {
		    		  String content = cNode.getLastChild().getTextContent().trim();
		    		 
		    		    sectionList.add(content);
		    		  }
		    	  }
		    
		      }
		}
	    catch (Exception e) {
			// TODO: handle exception
	    	e.printStackTrace();
		}
		
		
		
		return sectionList;
		
	}


    // HTML PARSER TO EXTRACT SECTION 

	public static String htmlParser(String what,String tFileLocation) {

		String divBlock = "";

	try {
		
		File input = new File(tFileLocation+"/master/master.html");

		 
		org.jsoup.nodes.Document doc = Jsoup.parse(input, "UTF-8", "");

		Elements block = doc.select("#" + what);

		
        divBlock= block.toString();  
		
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		return "no data found";
		
	}	 
		
		return divBlock;

	}
	
	
public static class Sectiondata {
		
		private String id;
		private int dropinsize;
		private List<String> sectionList = new ArrayList<String>();
		private Map<String, Object> dropIns =new HashMap<String, Object>();
		
		 public void setId(String id) {
		       this.id = id;
		    }
		
		 public void setSectionList(List<String> sectionList) {
		       this.sectionList = sectionList;
		    }
		 
		 public String getId(){
			 
			 return id;
			 
		 }
		 
		 public List<String> getsectionList(){
			 
			 return sectionList;
		 }
		 
		 public void setDropinsize(int dropinsize) {
		       this.dropinsize = dropinsize;
		    }
		 
		 public  int getDropinsize() {
		       return dropinsize;
		    }
		 
		 public void setDropins( Map<String, Object> dropIns){
			 
			 this.dropIns = dropIns;
		 }
		 
        public  Map<String, Object> getDropins(){
			 
        	
			 return dropIns;
		 }
	}

	
	
}
