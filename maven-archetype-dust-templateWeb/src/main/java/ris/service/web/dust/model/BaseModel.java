package ris.service.web.dust.model;

/**
 * This is a Base model object which will be extended by all other model objects.
 * @author Satish Gudluru
 *
 */
public class BaseModel {
	/**
	 * The contract ID.
	 */
	int contractId;

	/**
	 * Getter method.
	 * @return
	 */
	public int getContractId() {
		return contractId;
	}

	/**
	 * Setter method for contract ID
	 * @param contractId
	 */
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	

}
