package ris.service.web.dust.services;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.client.RestTemplate;

import com.principal.ris.annuity.bank.domain.Bank;
import com.principal.ris.annuity.bank.domain.BankInformation;
import com.principal.ris.annuity.communication.domain.Communication;
import com.principal.ris.annuity.communication.domain.MailingAddress;
import com.principal.ris.annuity.contract.domain.Contract;
import com.principal.ris.annuity.disbursement.domain.Disbursement;
import com.principal.ris.annuity.request.authorization.domain.RequestAuthorization;
import com.principal.ris.annuity.tax.domain.Tax;

import ris.service.web.dust.model.ContractInfoModel;
import ris.service.web.dust.model.CustomerInfoModel;
import ris.service.web.dust.model.DeliveryMethodModel;
import ris.service.web.dust.model.FinancialTransactionsModel;
import ris.service.web.dust.model.FlexibleWithDrawalProcessInfoModel;
import ris.service.web.dust.model.ImportantNotesModel;
import ris.service.web.dust.model.NotesModel;
import ris.service.web.dust.model.ReqAuthorizationModel;
import ris.service.web.dust.model.TaxInfoModel;

/**
 * This is an implementation of Iservice and it serves the requests from FWO handlers 
 * which will communicate with rest end points or rules engine  and fetch the 
 * corresponding domain model object. Then populate section specific model objects. 
 * 
 * @author Satish Gudluru
 *
 */
public class FWOService implements IService {

	private static final Log LOG = LogFactory.getLog(FWOService.class);

	@Override
	public ContractInfoModel getContractInfo(int contractId) {

		LOG.debug("getContractInfo - BEGIN");
		RestTemplate restTemplate = new RestTemplate();		
		String url= getRestEndPoint(contractId, Messages.CONTRACTINFO_ENDPOINT);	
		
		LOG.debug("Calling Rest End Point -->"+url);
		Contract contractObj = restTemplate.getForObject(url, Contract.class);		
		// Populate the Model object
		ContractInfoModel model = new ContractInfoModel();		
		
		int contractIdTmp = 0;		
		try{
			contractIdTmp = Integer.parseInt(contractObj.getContractId());
		}catch(NumberFormatException ne){
			LOG.error(ne);;
		}
		
		model.setContractId(contractIdTmp);
		// TBD - Set all the other model attributes here.
		LOG.debug("getContractInfo - END");
		return model;
	}

	@Override
	public CustomerInfoModel getCustomerInfo(int contractId) {
		
		LOG.debug("getCustomerInfo - BEGIN");
		RestTemplate restTemplate = new RestTemplate();		
		String url=null; 
		url = getRestEndPoint(contractId, Messages.COMMUNICATION_ENDPOINT);		
		Communication communicationObj = restTemplate.getForObject(url, Communication.class);	
		// Populate the Model object
		CustomerInfoModel model = new CustomerInfoModel();	
		
		int contractIdTmp = 0;		
		try{
			contractIdTmp = Integer.parseInt(communicationObj.getContractId());
		}catch(NumberFormatException ne){
			LOG.error(ne);;
		}
		model.setContractId(contractIdTmp);
		LOG.debug("getCustomerInfo - END");
		return model;
	}


	@Override
	public DeliveryMethodModel getDeliveryMethodInfo(int contractId) {
		
		LOG.debug("getDeliveryMethodInfo - BEGIN");
		RestTemplate restTemplate = new RestTemplate();		
		String url=null; 
		url = getRestEndPoint(contractId, Messages.DISBURSMENT_ENDPOINT);		
		Disbursement disbursementObj = restTemplate.getForObject(url, Disbursement.class);	
		// Populate the Model object
		DeliveryMethodModel model = new DeliveryMethodModel();	

		if(disbursementObj != null){
			int contractIdTmp = 0;
			try{
				contractIdTmp = Integer.parseInt(disbursementObj.getContractId());
			}catch(NumberFormatException ne){
				LOG.error(ne);;
			}
			model.setContractId(contractIdTmp);
			model.setPayeeContractNumber(disbursementObj.getPayeeContractNumber());
			model.setPayeeAccountNumber(disbursementObj.getPayeeAccountNumber());

		}
		// Get Bank Info
		url = getRestEndPoint(contractId, Messages.BANKINFO_ENDPOINT);		
		Bank banktObj = restTemplate.getForObject(url, Bank.class);	
		if(banktObj != null){
			List<BankInformation> bankInfoList = (banktObj.getBankDetails());
			if(bankInfoList != null){
				BankInformation bankInfo = bankInfoList.get(0);
				if(bankInfo != null){
					model.setRoutingNumber(bankInfo.getRoutingNumber());
					model.setAccountNumber(bankInfo.getAccountNumber());
				}
			}

			model.setAccountHolder(banktObj.getAccountHolder());
			model.setJointAccountHolder(banktObj.getJointAccountHolder());
		}
		// Get Communication Info
		url = getRestEndPoint(contractId, Messages.COMMUNICATION_ENDPOINT);		
		Communication communicationtObj = restTemplate.getForObject(url, Communication.class);

		if(communicationtObj != null){
			StringBuilder address = new StringBuilder();
			List<MailingAddress> mailAddressList = communicationtObj.getMailingAddresses();
			if(mailAddressList != null){
				MailingAddress mailAddress = mailAddressList.get(0);

				if(mailAddress != null){
					String streetName = mailAddress.getStreet1Name();
					if (streetName != null && ! "".equals(streetName)){
						address.append(streetName);
					}
					address.append(", "+mailAddress.getCityName());
					address.append(", "+mailAddress.getStateAbbreviation());
					address.append(", "+mailAddress.getFormattedPostalCode().substring(0, 5));
				}

				model.setAddress(address.toString());
			}
		}
		// TBD - set all other attributes pending 
		//		model.setDeliveryMethodRequest(deliveryMethodRequest);
		//		model.setCustomerRequestOwnerMailingAddressIndicator(customerRequestOwnerMailingAddressIndicator);
		//		model.setSigneeAccessibilityToOwnerFinancialAccountCode(signeeAccessibilityToOwnerFinancialAccountCode);
		//		model.setBankAccountType(bankAccountType);

		LOG.debug("getDeliveryMethodInfo - END");
		return model;
	}

	@Override
	public FinancialTransactionsModel getFinancialTransactions(int contractId) {		
		
		LOG.debug("getFinancialTransactions - BEGIN");
		RestTemplate restTemplate = new RestTemplate();		
		String url=null; 
		url = getRestEndPoint(contractId, Messages.DISBURSMENT_ENDPOINT);		
		Disbursement disbursementObj = restTemplate.getForObject(url, Disbursement.class);	
		// Populate the Model object
		FinancialTransactionsModel model = new FinancialTransactionsModel();	

		int contractIdTmp = 0;
		try{
			contractIdTmp = Integer.parseInt(disbursementObj.getContractId());
		}catch(NumberFormatException ne){
			LOG.error(ne);;
		}

		model.setContractId(contractIdTmp);
		// TBD - set all other attributes here
		LOG.debug("getFinancialTransactions - END");
		return model;
	}

	@Override
	public ReqAuthorizationModel getReqAuthorizationInfo(int contractId) {
		
		LOG.debug("getReqAuthorizationInfo - BEGIN");
		RestTemplate restTemplate = new RestTemplate();		
		String url=null; 
		url = getRestEndPoint(contractId, Messages.DISBURSMENT_ENDPOINT);		
		RequestAuthorization rauObj = restTemplate.getForObject(url, RequestAuthorization.class);	
		// Populate the Model object
		ReqAuthorizationModel model = new ReqAuthorizationModel();
		model.setJointOwnerSigneeDocReceiptIndicator(rauObj.getJointOwnerSigneeDocReceiptIndicator());

		// TBD - set all other attributes here
		LOG.debug("getReqAuthorizationInfo - END");
		return model;
	}

	@Override
	public TaxInfoModel getTaxInfo(int contractId) {

		LOG.debug("getTaxInfo - BEGIN");
		RestTemplate restTemplate = new RestTemplate();		
		String url=null; 
		url = getRestEndPoint(contractId, Messages.TAXINFO_ENDPOINT);		
		Tax taxObj = restTemplate.getForObject(url, Tax.class);	
		// Populate the Model object
		TaxInfoModel model = new TaxInfoModel();		
		
		model.setBackupFedWithholding(taxObj.isBackupFedWithholding());
		// TBD - set all other attributes here
		
		LOG.debug("getTaxInfo - END");
		return model;

	}


	@Override
	public FlexibleWithDrawalProcessInfoModel getFlexibleWithDrawlProcessInfo(int contractId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportantNotesModel getImportantNotes(int contractId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NotesModel getNotes(int contractId) {
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * This method id used to read the property file to get the rest end points.
	 * @param contractId, The contract ID which will be replace in the place holder in the end point URI.
	 * @param restEndPoint, the name of the rest end point.
	 * @return The the URL of the rest end point
	 */
	private String getRestEndPoint(int contractId, String restEndPoint) {
		
		LOG.debug("getRestEndPoint - BEGIN");
		String url = null;
		Properties props = new Properties();			

		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream("rest_endpoints.properties");			
			props.load(input);
		} catch (IOException e) {			
			LOG.error("Could not load the rest endpoints properties");
		}	
		String endPoint = props.getProperty(restEndPoint,"");
		
		if(endPoint != null && !endPoint.isEmpty()){
			url = MessageFormat.format(endPoint, contractId);
		}else{
			LOG.error("Rest end point cant be null or empty. please check rest_endpoints.properties");
			// TBD - Error page can be redirected here
		}
		LOG.debug("getRestEndPoint - END");
		return url;
	}


}
