package ris.service.web.dust.controller;

import java.io.File;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.principal.ris.annuity.request.authorization.domain.RequestAuthorization;

import ris.service.web.dust.model.ContractInfoModel;
import ris.service.web.dust.model.CustomerInfoModel;
import ris.service.web.dust.model.DeliveryMethodModel;
import ris.service.web.dust.model.FinancialTransactionsModel;
import ris.service.web.dust.model.ReqAuthorizationModel;
import ris.service.web.dust.model.TaxInfoModel;
import ris.service.web.dust.services.AdminLauncherServicesEnum;
import ris.service.web.dust.services.IService;
import ris.service.web.dust.services.ServiceFactory;


/**
 * This is a controller to handle the requests from FWO page for various sections
 * and that will map HTTP requests to given methods with 
 * matching @RequestMapping URIs.  In this case, all requests that start with "/fwo" will be 
 * mapped to this controller.
 * 
 * @author Satish Gudluru
 *
 */
@Controller
@RequestMapping(value="/fwo")
public class FWOController {	
	
	private static final Log LOG = LogFactory.getLog(SampleRestController.class);
	IService service = ServiceFactory.createService(AdminLauncherServicesEnum.FWOService);
	
	/**
	 * Handler method to retrieve the Contract information with /contractinfo/{contractId} URI. 
	 * It fetches the information from rest services and populates ContractInfoModel object.
	 * @param contractId, ContractID
	 * @param model, Spring Model Object.
	 * @return ContractInfoModel.
	 * @throws Exception - Exception.
	 */
	@RequestMapping(value="/contractinfo/{contractId}", 
			method=RequestMethod.GET)
	public ContractInfoModel retrieveContractInfo(@PathVariable String contractId, Model model) throws Exception {
		LOG.debug("Fetching Contract Info for the contract Id:"+contractId);
		ContractInfoModel contractModel = service.getContractInfo(Integer.parseInt(contractId));
		
		return contractModel;
	}
	
	/**
	 * Handler method to retrieve the Contract information with /contractinfo/{contractId} URI.
	 * It fetches the information from rest services and populates CustomerInfoModel object and return to UI
	 * @param contractId, ContractID
	 * @param model, Spring Model Object.
	 * @return ContractInfoModel.
	 * @throws Exception - Exception
	 */
	@RequestMapping(value="/customerinfo/{contractId}", 
			method=RequestMethod.GET)
	public CustomerInfoModel retrieveCustomerInfo(@PathVariable String contractId, Model model) throws Exception {
		LOG.debug("Retreiving customer info for contract Id:"+contractId);
		CustomerInfoModel cInfoModel = service.getCustomerInfo(Integer.parseInt(contractId));		
		return cInfoModel;
	}
	
	/**
	 * Handler method to retrieve the Contract information with /deliverymethodinfo/{contractId} URI. 
	 * It fetches the information from rest services and populates DeliveryMethodModel object and return to UI.
	 * @param contractId, ContractID
	 * @param model, Spring Model Object.
	 * @return ContractInfoModel.
	 * @throws Exception - Exception
	 */
	@RequestMapping(value="/deliverymethodinfo/{contractId}", 
			method=RequestMethod.GET)
	public DeliveryMethodModel retrieveDeliveryMethodResource(@PathVariable String contractId, Model model) throws Exception {		
		LOG.debug("Retreiving the DeliveryMethod Info for contract Id:"+contractId);
		DeliveryMethodModel DMInfoModel = service.getDeliveryMethodInfo(Integer.parseInt(contractId));		
		return DMInfoModel;
	}
	/**
	 * Handler method to retrieve the Contract information with /taxinfo/{contractId} URI.
	 * It fetches the information from rest services and populates TaxInfoModel object and return to UI.
	 * @param contractId, ContractID
	 * @param model, Spring Model Object.
	 * @return ContractInfoModel.
	 * @throws Exception - Exception
	 */	
	@RequestMapping(value="/taxinfo/{contractId}", 
			method=RequestMethod.GET)
	public TaxInfoModel retrieveTaxInfoResource(@PathVariable String contractId, Model model) throws Exception {
		LOG.debug("Retreiving Tax info for the contract Id:"+contractId);
		// TO - DO
		return null;
	}
	
	/**
	 * Handler method to retrieve the Contract information with /requestauth/{contractId} URI. 
	 * It fetches the information from rest services and populates ReqAuthorizationModel object and return to UI.
	 * @param contractId, ContractID
	 * @param model, Spring Model Object.
	 * @return ReqAuthorizationModel 
	 * @throws Exception - Exception
	 */		
	@RequestMapping(value="/requestauth/{contractId}", 
			method=RequestMethod.GET)
	public ReqAuthorizationModel retrieveRequestAuthorization(@PathVariable String contractId, Model model) throws Exception {
		
		LOG.debug("Retreiving the Request Authorization info for the contract Id:"+contractId);		
		return null;
	}
	
	
	/**
	 * Handler method to retrieve the Contract information with /financialtransactions/{contractId} URI. 
	 * It fetches the information from rest services and populates FinancialTransactionsModel object and return to UI.
	 * @param contractId, ContractId
	 * @param model, Spring Model Object.
	 * @return FinancialTransactionsModel -  and returns.
	 * @throws Exception - Exception
	 */
	@RequestMapping(value="/financialtransactions/{contractId}", 
			method=RequestMethod.GET)
	public FinancialTransactionsModel retrieveFinancialTransactions(@PathVariable String contractId, Model model) throws Exception {
		LOG.debug(" Retreiving Financial transactions info for the contract Id:"+contractId);		
		// TO DO
		return null;
	}
	
	
}