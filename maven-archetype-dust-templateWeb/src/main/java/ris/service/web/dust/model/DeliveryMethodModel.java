package ris.service.web.dust.model;

import com.principal.ris.annuity.editable.bank.domain.BankAccountTypeRequest;
import com.principal.ris.annuity.request.authorization.domain.MailingInstructions;
import com.principal.ris.annuity.request.fwo.domain.DeliveryMethodRequest;

/**
 * This is a model object for Delivery method Information.
 * @author Satish Gudluru
 *
 */
public class DeliveryMethodModel extends BaseModel {
	
	//FlexibleWithdrawalOptionRequest.disbursementEditable.deliveryMethodRequest
	private DeliveryMethodRequest deliveryMethodRequest; 
	
	// FlexibleWithdrawalOptionRequest.requestAuthorization.customerRequestOwnerMailingAddressIndicator
	private MailingInstructions customerRequestOwnerMailingAddressIndicator; 
	
	//FlexibleWithdrawalOptionRequest.requestAuthorization.signeeAccessibilityToOwnerFinancialAccountCode
	private Boolean signeeAccessibilityToOwnerFinancialAccountCode; 
	
	//FlexibleWithdrawalOption.bankEditable.bankAccountType
	private BankAccountTypeRequest bankAccountType; 

	//Communication.mailingAddresses.get(0):	Use street*Name if not "" or null.	cityName, stateAbbreviation, First 5 digits of formattedPostalCode
	private String address; 
	
	private String routingNumber; //Bank.bankInformation.routingNumber
	
	private String accountNumber;//Bank.bankInformation.accountNumber
	
	private String accountHolder; //Bank.accountHolder
	
	private String jointAccountHolder; //Bank.jointAccountHolder
	
	private String payeeContractNumber; //Disbursement.payeeContractNumber
	
	private String payeeAccountNumber; //Disbursement.payeeAccountNumber
	
	
	public DeliveryMethodRequest getDeliveryMethodRequest() {
		return deliveryMethodRequest;
	}
	
	public void setDeliveryMethodRequest(DeliveryMethodRequest deliveryMethodRequest) {
		this.deliveryMethodRequest = deliveryMethodRequest;
	}
	
	public MailingInstructions getCustomerRequestOwnerMailingAddressIndicator() {
		return customerRequestOwnerMailingAddressIndicator;
	}
	
	public void setCustomerRequestOwnerMailingAddressIndicator(
			MailingInstructions customerRequestOwnerMailingAddressIndicator) {
		this.customerRequestOwnerMailingAddressIndicator = customerRequestOwnerMailingAddressIndicator;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public BankAccountTypeRequest getBankAccountType() {
		return bankAccountType;
	}
	
	public void setBankAccountType(BankAccountTypeRequest bankAccountType) {
		this.bankAccountType = bankAccountType;
	}
	
	public String getRoutingNumber() {
		return routingNumber;
	}
	
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	
	public String getJointAccountHolder() {
		return jointAccountHolder;
	}
	
	public void setJointAccountHolder(String jointAccountHolder) {
		this.jointAccountHolder = jointAccountHolder;
	}
	
	public Boolean getSigneeAccessibilityToOwnerFinancialAccountCode() {
		return signeeAccessibilityToOwnerFinancialAccountCode;
	}
	
	public void setSigneeAccessibilityToOwnerFinancialAccountCode(Boolean signeeAccessibilityToOwnerFinancialAccountCode) {
		this.signeeAccessibilityToOwnerFinancialAccountCode = signeeAccessibilityToOwnerFinancialAccountCode;
	}
	
	public String getPayeeContractNumber() {
		return payeeContractNumber;
	}
	
	public void setPayeeContractNumber(String payeeContractNumber) {
		this.payeeContractNumber = payeeContractNumber;
	}
	
	public String getPayeeAccountNumber() {
		return payeeAccountNumber;
	}
	
	public void setPayeeAccountNumber(String payeeAccountNumber) {
		this.payeeAccountNumber = payeeAccountNumber;
	}
	
	

}
