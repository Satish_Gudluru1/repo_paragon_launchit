package ris.service.web.dust.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.sf.json.JSONObject;

/**
 * This is a sample {@link RestController} that will map HTTP requests to given methods with 
 * matching @RequestMapping URIs.  In this case, all requests that start with "/sample" will be 
 * mapped to this controller, and further URI node mappings will be specified at the method 
 * level (see {@link #retrieveSample(String, String)}).
 */
@Transactional
@Controller
@RequestMapping(value="/pfg")
public class PFGAdminController {	
	
	private static final Log LOG = LogFactory.getLog(SampleRestController.class);
	
	
	@RequestMapping(value="/login", 
			method=RequestMethod.GET, 
			produces={MediaType.TEXT_HTML_VALUE})
	public String retrieveLogin(Model model) throws Exception {
		
		return "/pfg/login";
	}
	
	@RequestMapping(value="/fwoprocess", 
			method=RequestMethod.POST, 
			produces={MediaType.TEXT_HTML_VALUE})
	public String retrieveSample(Model model) throws Exception {
		
		String[] ftdropdown = {"Set Up FWO","Update FWO","Stop FWO","Termination","Partial Withdrawal"};		
		model.addAttribute("ftdropdown",ftdropdown);		
		JSONObject obj=new JSONObject();
		obj.put("check","checked");
		model.addAttribute(obj);			
		return "/pfg/fwoprocess";
	}
}