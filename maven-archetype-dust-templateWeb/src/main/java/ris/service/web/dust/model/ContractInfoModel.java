package ris.service.web.dust.model;

import java.util.List;

/**
 * This is model object for Contract Info
 * @author Satish Gudluru
 *
 */
public class ContractInfoModel extends BaseModel {
	
	private List<String> contractIdsList; // BasicRequest.contractIdsList
	
	private String prodName ; //Product.productSystemText + "-" + Product.product LongText	
	
	private String taxQualificationStatus; //Contract.taxQualificationStatus.getName();
	
	private String title;
	
	public List<String> getContractIdsList() {
		return contractIdsList;
	}
	
	public void setContractIdsList(List<String> contractIdsList) {
		this.contractIdsList = contractIdsList;
	}
	
	public String getProdName() {
		return prodName;
	}
	
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	public String getTaxQualificationStatus() {
		return taxQualificationStatus;
	}
	
	public void setTaxQualificationStatus(String taxQualificationStatus) {
		this.taxQualificationStatus = taxQualificationStatus;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

}
