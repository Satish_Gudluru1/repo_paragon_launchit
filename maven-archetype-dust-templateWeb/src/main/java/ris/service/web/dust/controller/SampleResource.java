package ris.service.web.dust.controller;

import ris.service.web.dust.Resource;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Represents a sample {@link Resource} object.  
 * 
 * We never want to directly expose a regular 
 * object via REST since the objects we expose via REST typically need to have web based 
 * information in them, such as how the object needs to be represented in JSON format, how 
 * to format error messages, or the addition of attributes to facilitate RESTful semantics.
 * 
 * A consequence of this will often be what appears to be redundancy between a {@link Resource} 
 * and a domain or service object.  Please do not shortcut this because you think its 
 * redundancy - follow this pattern.
 */
public class SampleResource extends Resource {

	private String contract;
	
	/**
	 * Here is an example where you could use a Jackson annotation to change the To/From mapping of 
	 * this field when the format is JSON.  This will output this field as "participantId", as well 
	 * as convert incoming JSON of "participantId" into this field.
	 */
	@JsonProperty("participantId")
	private String memberId;
	
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
}
