package ris.service.web.dust.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class is factory to create the service for individual processes based on their name.
 * @author Satish Gudluru
 *
 */
public class ServiceFactory {
	
	private static final Log LOG = LogFactory.getLog(FWOService.class);
	
	/**
	 * This method creates the service based on the service name that has been requested.
	 * @param service_name, The name of the service
	 * @return , IServce
	 */
	public static IService createService(AdminLauncherServicesEnum service_name) {

		LOG.debug("Creating service for:"+service_name);
		IService service = null;		
		switch(service_name){
		case FWOService:
			service = new FWOService();
			break;
		case PartialWithdrawalService:
			service = null; // TBD
			break;
		case TerminationService:
			service = null; // TBD
			break;
		case ReplacementService:
			service = null; // TBD
			break;
		}
		return service;

	}

}
