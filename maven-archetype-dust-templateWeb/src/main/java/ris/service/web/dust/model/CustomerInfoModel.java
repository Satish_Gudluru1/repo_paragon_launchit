package ris.service.web.dust.model;

/**
 * This is a model object for Customer Information.
 * @author Satish Gudluru
 *
 */
public class CustomerInfoModel extends BaseModel {
	
	private String owner = null;
	
	private String jointOwner = null;
	
	private int ownerClientID = 0;
	
	private int jointOwnerClientID =0;
	
	private String annuitantName;
	
	private  String jointAnnuitantName;
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public String getJointOwner() {
		return jointOwner;
	}
	
	public void setJointOwner(String jointOwner) {
		this.jointOwner = jointOwner;
	}
	
	public int getOwnerClientID() {
		return ownerClientID;
	}
	
	public void setOwnerClientID(int ownerClientID) {
		this.ownerClientID = ownerClientID;
	}
	
	public int getJointOwnerClientID() {
		return jointOwnerClientID;
	}
	
	public void setJointOwnerClientID(int jointOwnerClientID) {
		this.jointOwnerClientID = jointOwnerClientID;
	}

	public String getAnnuitantName() {
		return annuitantName;
	}

	public void setAnnuitantName(String annuitantName) {
		this.annuitantName = annuitantName;
	}

	public String getJointAnnuitantName() {
		return jointAnnuitantName;
	}

	public void setJointAnnuitantName(String jointAnnuitantName) {
		this.jointAnnuitantName = jointAnnuitantName;
	}
	

}
