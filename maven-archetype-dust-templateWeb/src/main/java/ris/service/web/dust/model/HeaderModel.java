package ris.service.web.dust.model;

/**
 * This is a model object for Header Details like title, product info etc..
 * @author Satish Gudluru
 *
 */
public class HeaderModel extends BaseModel{
	
	String title = null;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	// To be mapped with the fields form UI and Json
}
