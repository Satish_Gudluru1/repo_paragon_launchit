package ris.service.web.dust.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is a sample {@link RestController} that will map HTTP requests to given methods with 
 * matching @RequestMapping URIs.  In this case, all requests that start with "/sample" will be 
 * mapped to this controller, and further URI node mappings will be specified at the method 
 * level (see {@link #retrieveSample(String, String)}).
 */
@Transactional
@Controller
@RequestMapping(value="/sample")
public class SampleRestController {	
	
	private static final Log LOG = LogFactory.getLog(SampleRestController.class);
	
	/**
	 * This method is an example of mapping a HTTP GET request with a ContentType=appliction/json 
	 * to the "/sample" URI. It maps the required "contract_num" and "id_num" query string parameters 
	 * into a {@link SampleResource} object, then return a JSON representation of {@link SampleResource} in 
	 * a HTTP response back to the client.
	 * 
	 * This method demonstrates only one tiny sliver of all the REST operations and how mappings can 
	 * be done using Spring MVC.  Please do not assume that you should copy this example verbatim to 
	 * do REST.  This is likely a common pattern for a GET request, however, you will really want to 
	 * read more about the various REST operations, as well as all the different ways you can do 
	 * REST using Spring MVC.
	 * 
	 * @param contract
	 * @param memberId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="", 
			method=RequestMethod.GET, 
			produces={MediaType.APPLICATION_JSON_VALUE})
	public SampleResource retrieveSample(@RequestParam(value="contract_num", required=true) String contract,
										 @RequestParam(value="id_num", required=true) String memberId) throws Exception {
		
		LOG.debug("echoing REST request ...");
		
		SampleResource sampleResource = new SampleResource();
		sampleResource.setContract(contract);
		sampleResource.setMemberId(memberId);
			
		return sampleResource;
	}
	
	@RequestMapping(value="/name", 
			method=RequestMethod.GET, 
			produces={MediaType.TEXT_HTML_VALUE})
	public String retrieveSample(Model model) throws Exception {
		
		LOG.debug("echoing REST request ...");
		
		model.addAttribute("name", "Bob");
			
		return "/sample/name";
	}
	
	@RequestMapping(value="/index",
			method=RequestMethod.GET,
			produces={MediaType.APPLICATION_JSON_VALUE})
	public SampleResource retrieveSample() throws Exception {
		LOG.debug("Request start");
		SampleResource sample = new SampleResource();
		sample.setContract("123456");
		sample.setMemberId("987456321");
		return sample;
	}
}