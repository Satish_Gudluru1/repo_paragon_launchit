package ris.service.web.dust;

import static com.google.common.net.MediaType.HTML_UTF_8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.google.common.collect.Lists;
import com.principal.ris.web.servlet.view.dust.DustViewResolver;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {
		
	@Override
	protected void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.defaultContentType(MediaType.APPLICATION_JSON); 
		configurer.favorParameter(true);
		configurer.favorPathExtension(true);
		configurer.ignoreAcceptHeader(true);
		
		Map<String, MediaType> props = new HashMap<String, MediaType>();
		props.put("json", MediaType.APPLICATION_JSON);
		props.put("dust", MediaType.TEXT_HTML);
		configurer.mediaTypes(props);
	}
	
	@Bean
	@DependsOn("mvcContentNegotiationManager" )
	public ContentNegotiatingViewResolver contentNegotiatingViewResolver() throws Exception {
		ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
		contentNegotiatingViewResolver.setOrder(1);
		contentNegotiatingViewResolver.setContentNegotiationManager(mvcContentNegotiationManager());
		
		// set viewResolvers
		ViewResolver beanNameViewResolver = new BeanNameViewResolver();
		
		List<ViewResolver> viewResolvers = Lists.newArrayList(beanNameViewResolver); 
		contentNegotiatingViewResolver.setViewResolvers(viewResolvers);

		// set default views
		MappingJackson2JsonView jacksonView = new MappingJackson2JsonView();
		jacksonView.setExtractValueFromSingleKeyModel(true);
		
		// add all views to list
		List<View> defaultViews = new ArrayList<View>();
		defaultViews.add(jacksonView);
		
		contentNegotiatingViewResolver.setDefaultViews(defaultViews);
		
		return contentNegotiatingViewResolver;
	}
	
	@Bean
    public DustViewResolver dustViewResolver() {
		DustViewResolver dustJSViewResolver = new DustViewResolver();
        dustJSViewResolver.setOrder(2);
        dustJSViewResolver.setPrefix("/WEB-INF/");
        dustJSViewResolver.setSuffix(".dust");
        dustJSViewResolver.setContentType(HTML_UTF_8.toString());
        return dustJSViewResolver;
    }
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		if (!registry.hasMappingForPattern("/**")) {
			String[] locations = new String[1];
			locations[0] = "/assets/";
			registry.addResourceHandler("/**").addResourceLocations(locations);
		}
	}

}
