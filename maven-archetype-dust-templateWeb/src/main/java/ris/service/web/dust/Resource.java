package ris.service.web.dust;

import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;


/**
 * Base class Resource that all of our Resource objects will extend. This will allow
 * 	us to have a consistent pattern across all of Resource objects.
 * 
 * See basic usage of this object in the JUnit test: {@link ResourceTest} and {@link ExampleResource}.
 * 
 * This Resource object will give you a multi-map of messages to add informations,
 * 	warning, and/or exception messages. You can use the addMessage() method for 
 * 	simple adding of messages as well as a getter for retrieving the entire multi-map.
 * 
 * The list of messages for each key is a Ordered list (Using {@link LinkedList}) so
 * 	the order you add messages will be held.
 * 
 * A couple basic rules for handling messages:
 * 	* The key to message should match the Resource attribute name it ties to.
 * 	* If it is a message that relates to the entire Resource object you can give it
 * 		its own name or use the DEFAULT_ERROR_MESSAGE_KEY provided (key="errorMessages")
 * 
 * @author s724885
 */
public class Resource {
	
	/**
	 * Default/Generic key to use for error messages for the entire Resource object.
	 */
	public static final String DEFAULT_ERROR_MESSAGE_KEY = "errorMessages";
	
	@JsonProperty("_messages")
	private LinkedListMultimap<String, String> messages = LinkedListMultimap.create();
	
	/**
	 * Adds the given message to the messages multimap.
	 * 
	 * The list of messages for each key is a Ordered list (Using {@link LinkedList}) so
	 * 	the order you add messages will be held.
	 * 
	 * @param key
	 * @param message
	 */
	public void addMessage(String key, String message){
		messages.put(key, message);
	}
	
	/**
	 * Retrieve the entire {@link Multimap} of messages.
	 * 
	 * @return
	 */
	public LinkedListMultimap<String, String> getMessages(){
		return messages;
	}
}
