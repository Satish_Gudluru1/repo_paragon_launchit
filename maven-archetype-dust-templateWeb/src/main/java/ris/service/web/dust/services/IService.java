package ris.service.web.dust.services;

import ris.service.web.dust.model.ContractInfoModel;
import ris.service.web.dust.model.CustomerInfoModel;
import ris.service.web.dust.model.DeliveryMethodModel;
import ris.service.web.dust.model.FinancialTransactionsModel;
import ris.service.web.dust.model.FlexibleWithDrawalProcessInfoModel;
import ris.service.web.dust.model.ImportantNotesModel;
import ris.service.web.dust.model.NotesModel;
import ris.service.web.dust.model.ReqAuthorizationModel;
import ris.service.web.dust.model.TaxInfoModel;

/**
 * This is an interface which defines the api for retrieving the section information for all the sections for various processes.
 * @author Satish Gudluru
 *
 */
public interface IService {
	
	/**
	 * This method is used to get the contract information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return ContractInfoModel
	 */
	ContractInfoModel getContractInfo(int contractId);
	
	/**
	 * This method is used to get the customer information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return CustomerInfoModel
	 */
	CustomerInfoModel getCustomerInfo(int contractId);
	
	/**
	 * This method is used to get the Request Authorization information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return ReqAuthorizationModel
	 */
	ReqAuthorizationModel getReqAuthorizationInfo(int contractId);
	
	/**
	 * This method is used to get the Financial Transactions information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return FinancialTransactionsModel
	 */
	FinancialTransactionsModel getFinancialTransactions(int contractId);
	
	/**
	 * This method is used to get the FlexibleWith Drawl Process information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return FlexibleWithDrawalProcessInfoModel
	 */
	FlexibleWithDrawalProcessInfoModel getFlexibleWithDrawlProcessInfo(int contractId);
	
	/**
	 * This method is used to get the Important Notes information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return ImportantNotesModel
	 */
	ImportantNotesModel getImportantNotes(int contractId);
	

	/**
	 * This method is used to get the delivery method information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return DeliveryMethodModel
	 */
	DeliveryMethodModel getDeliveryMethodInfo(int contractId);
		
	/**
	 * This method is used to get the Notes information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return NotesModel
	 */
	NotesModel getNotes(int contractId);
		
	/**
	 * This method is used to get the Tax information to the given contract ID.
	 * @param contractId, The contract ID to which the information should be retrieved.
	 * @return TaxInfoModel
	 */
	TaxInfoModel getTaxInfo(int contractId);

}
