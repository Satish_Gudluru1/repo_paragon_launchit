package ris.service.web.dust.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is a Controller that will map HTTP requests to given methods with 
 * matching @RequestMapping URIs.  In this case, all requests that start with "/process" will be 
 * mapped to this controller, and further URI node mappings will be specified at the method 
 * level.
 */

/**
 * @author Satish Gudluru
 *
 */
@Controller
@RequestMapping(value="/process")
public class AdminLauncherController {	
	
	private static final Log LOG = LogFactory.getLog(SampleRestController.class);
	
	
	@RequestMapping(value="/fwoprocess", 
			method=RequestMethod.POST, 
			produces={MediaType.TEXT_HTML_VALUE})
	public String retrieveFWOProcess(Model model) throws Exception {
		
		return "/process/fwoprocess";
	}
	
	@RequestMapping(value="/fwofollowup", 
			method=RequestMethod.POST, 
			produces={MediaType.TEXT_HTML_VALUE})
	public String retrieveFWOFollowup(Model model) throws Exception {		
	
		return "/process/fwoprocess";
	}
	
	
	
}