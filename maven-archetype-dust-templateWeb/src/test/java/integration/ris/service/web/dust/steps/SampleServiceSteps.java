package integration.ris.service.web.dust.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import integration.ris.service.web.dust.BaseSpringIT;

import org.springframework.beans.factory.annotation.Autowired;

import ris.service.web.dust.controller.SampleResource;
import ris.service.web.dust.controller.SampleRestController;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SampleServiceSteps extends BaseSpringIT{

	@Autowired
	private SampleRestController sampleRestController;
	
	//Inputs
	private String contract;
	private String memberId;
	//Outputs
	private SampleResource sampleResource;
	
	@Before
	public void setup(){
		contract = null;
		memberId = null;
		sampleResource = null;
	}
	
	@Given("^a contract number of \"(.*?)\"$")
	public void a_contract_number_of(String contract) throws Throwable {
	    this.contract = contract;
	}

	@Given("^a member id of \"(.*?)\"$")
	public void a_member_id_of(String memberId) throws Throwable {
	    this.memberId = memberId;
	}

	@When("^a request is made to the /sample$")
	public void a_request_is_made_to_the_sample() throws Throwable {
	    sampleResource = sampleRestController.retrieveSample(contract, memberId);
	}

	@Then("^the contract will be \"(.*?)\"$")
	public void the_contract_will_be(String contract) throws Throwable {
	    assertNotNull(sampleResource);
	    assertNotNull(sampleResource.getContract());
	    assertEquals(contract, sampleResource.getContract());
	}

	@Then("^the member id will be \"(.*?)\"$")
	public void the_member_id_will_be(String memberId) throws Throwable {
		assertNotNull(sampleResource);
	    assertNotNull(sampleResource.getMemberId());
	    assertEquals(memberId, sampleResource.getMemberId());
	}
}
