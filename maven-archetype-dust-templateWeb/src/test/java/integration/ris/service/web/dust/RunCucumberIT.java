package integration.ris.service.web.dust;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import ris.service.web.test.utility.TestEnvironment;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Class for kicking off Cucumber tests.
 * 
 * @author A123456
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(tags = "~@wip", 
	glue = {"integration.ris.service.web.dust", "cucumber.api.spring"}, 
	plugin = { "pretty", "json:target/cucumber.json" })
public class RunCucumberIT {
	
	@BeforeClass
	public static void environmentSetup(){
		TestEnvironment.PILOT.setUp();
	}

}
