package integration.ris.service.web.dust;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ris.service.web.dust.Application;
import ris.service.web.dust.controller.SampleRestController;
import ris.service.web.test.utility.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
@WebAppConfiguration("file:WebContent")
@ActiveProfiles(profiles={"embedded"})
@ContextConfiguration(classes={Application.class, ApplicationTest.class}, loader=SpringApplicationContextLoader.class)
public class BaseSpringIT {
	
	@Autowired
	private SampleRestController sampleRestController;

	@Test
	public void test() {
		assertNotNull(sampleRestController);
	}
	
}