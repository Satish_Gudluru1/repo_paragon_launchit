package ris.service.web.test.utility;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Helper class for setting up the DB2 environment you plan on connecting to.  We want to allow
 * overrides in the 'JRE' tab of a unit test or on the build server, but also allow a developer
 * to specify their own "defaults"  (rather than defaulting to Test DB2)
 * 
 * To use this class, add a method to your unit test like the following:
 * 
 * <pre><code>
 	&#64;BeforeClass
	public static void setUp() {
		TestEnvironment.REGRESSION.setUp();
	}
	</code></pre>
 * 
 * 
 *
 */
public enum TestEnvironment {
	TEST(Constants.SERVER_ENVIRONMENT_TEST, Constants.SCHEMA_DB2),
	PILOT(Constants.SERVER_ENVIRONMENT_PILOT, Constants.SCHEMA_DB2),
	TLOT(Constants.SERVER_ENVIRONMENT_TEST, "DB2PRD"),
	TPOT(Constants.SERVER_ENVIRONMENT_TEST, "DB2PLT"),
	REGRESSION(Constants.SERVER_ENVIRONMENT_PILOT, "DB2RGT");
	
	private static final Log LOG = LogFactory.getLog(TestEnvironment.class);
	private static final String SERVER_ENVIRONMENT_VARIABLE = "ServerEnvironment";
	private static final String SCHEMA_ENVIRONMENT_VARIABLE = "ris.schema";
	
	private final String serverEnvironment;
	private final String schema;
	TestEnvironment(String serverEnvironment, String schema) {
		this.serverEnvironment = serverEnvironment;
		this.schema = schema;
	}
	
	/**
	 * If no system variables are already set, then set them.  Otherwise, ignore 
	 */
	public void setUp() {
		String systemServerEnvironment = System.getProperty(SERVER_ENVIRONMENT_VARIABLE);
		String systemSchema = System.getProperty(SCHEMA_ENVIRONMENT_VARIABLE);
		if(StringUtils.isBlank(systemServerEnvironment) && StringUtils.isBlank(systemSchema)) {
			LOG.info("Setting ServerEnvironment to " + serverEnvironment + " and schema to " + schema);
			System.setProperty(SERVER_ENVIRONMENT_VARIABLE, this.serverEnvironment);
			System.setProperty(SCHEMA_ENVIRONMENT_VARIABLE, this.schema);
		} else {
			LOG.warn("Using system variables for server environment (" + systemServerEnvironment + ") and schema (" + systemSchema + ")");
		}
		
	}
	
	private static final class Constants {
		private Constants() {
			//hide public constructor
		}
		private static final String SERVER_ENVIRONMENT_TEST = "Shared_Test";
		private static final String SERVER_ENVIRONMENT_PILOT = "Pilot";
		private static final String SCHEMA_DB2 = "DB2";
		
	}

}