package ris.service.web.test.utility;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApplicationTest {
	
	/**
	 * Need mocked transaciton manager to run Cucumber Tests. 
	 * @return
	 */
	@Bean(name = "transactionManager")
	public MockedTransactionManager mockedTransactionManager(){
		return new MockedTransactionManager();
	}
}
