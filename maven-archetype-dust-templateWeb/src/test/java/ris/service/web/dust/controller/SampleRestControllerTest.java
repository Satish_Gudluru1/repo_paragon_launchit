package ris.service.web.dust.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

public class SampleRestControllerTest {
	
	private MockMvc mockMvc; 
	private String testContract = "5301201";
	
	@Before
    public void setup() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	    viewResolver.setPrefix("/WebContent/WEB-INF/");
	    viewResolver.setSuffix(".dust");
        mockMvc = MockMvcBuilders.standaloneSetup(new SampleRestController())
                                 .setViewResolvers(viewResolver)
                                 .build();
    }
	
	@Test
	public void test_GET_sample_index() throws Exception {
		this.mockMvc
			.perform(get("/sample/index?contract=" + testContract).contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andDo(MockMvcResultHandlers.print())
			.andExpect(view().name(("sample/index")))
			.andExpect(model().attributeExists("sampleResource"));
	}
	
	@Test
	public void test_GET_sample_name() throws Exception {
		
		this.mockMvc
			.perform(get("/sample/name").contentType(MediaType.TEXT_HTML_VALUE))
			.andExpect(status().isOk())
			.andDo(MockMvcResultHandlers.print())
			.andExpect(view().name(("/sample/name")))
			.andExpect(model().attributeExists("name"));
	}
		
	
	@Test
	public void test_PUT_sample_invalidOperation() throws Exception {
		
		this.mockMvc
			.perform(put("/sample/index").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isMethodNotAllowed())
			.andDo(MockMvcResultHandlers.print());
	}

	/**
	 * Showing that you can write a normal unit test at controller methods.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_retrieveSample() throws Exception{
		SampleRestController sampleRestController = new SampleRestController();
		SampleResource sampleResource = sampleRestController.retrieveSample("123456", "123456789");
		assertNotNull(sampleResource);
		assertEquals("123456", sampleResource.getContract());
		assertEquals("123456789", sampleResource.getMemberId());
	}
}
