@txn
Feature: Test Scenarios for Sample Rest Controller

 Scenario: /sample call with contract and id
  Given a contract number of "12345"
  And a member id of "123456789"
  When a request is made to the /sample
  Then the contract will be "12345"
  And the member id will be "123456789"