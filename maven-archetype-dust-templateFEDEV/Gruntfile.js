module.exports = function(grunt) {
	var date = new Date();
	
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json")
	});
	
	// import task configs from grunt folder
	grunt.loadTasks("grunt");

	grunt.loadTasks("node_modules/fe-grunt-config-dust/Dust");

	grunt.loadNpmTasks("grunt-extend-config");

	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json")
	});

	grunt.loadTasks("node_modules/fe-grunt-config-dust/Dust");

	grunt.extendConfig({
		replace: {
			javadev: {
				src: ["dist/**/*.js"],
				overwrite: true,
				replacements: [{
					from: "{contextRoot}",
					to: "/dust"
				}]
			},
			nodedev: {
				src: ["dist/**/*.js"],
				overwrite: true,
				replacements: [{
					from: "{contextRoot}",
					to: "/dist/template/data"
				}]
			}
		}
	});	

	grunt.loadNpmTasks("grunt-text-replace");
};