import {BaseComponent} from "femur-risui";
import View from "./example-view";
import Model from "./example-model";

export default class ExampleComponent extends BaseComponent {
	constructor({el}) {
		super({
			View,
			Model,
			fetchOnLoad: true,
			renderOnLoad: true,
			url: "{contextRoot}/sample/index.json",
			el
		});
	}
};