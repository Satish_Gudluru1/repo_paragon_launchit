import {BaseModel, set} from "femur-risui";

@set({
	/*defaults: controllerData.sampleResource*/
	defaults : {
		name: "Example Name"
	}
})
export default class ExampleModel extends BaseModel {

	/*computedAttributes: {}*/
	/**
	 * Parse is used to manipulate data before it
	 * gets set to the model. An example might be
	 * to parse a timestamp into a date object.
	 * The "data" variable is all the starting data
	 * and be sure to return the same object after
	 * it has been manipulated
	 * @param {Object} data - Model's starting data
	 * @return {Object} data - Model's modified data
	 */
	parse(data) {
		return data;
	}
};