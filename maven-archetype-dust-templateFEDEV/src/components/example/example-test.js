import QUnit from "qunitjs";
import Model from "./example-model";

QUnit.test("Example Test", function(assert) {
	var model = new Model();

	assert.equal(model.get("name"), "Example Name", "The example name is correct");
});