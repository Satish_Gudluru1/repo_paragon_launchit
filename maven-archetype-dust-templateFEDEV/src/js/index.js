import {App} from "femur-risui";
import Example from "../components/example/example-component";

$(function() {
	var app = window.app = new App({
		components: {
			example: Example
		}
	});
	app.start();
});