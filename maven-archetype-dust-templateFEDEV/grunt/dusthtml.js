module.exports = function(grunt) {
	grunt.config.set("dusthtml", {
		dev: {
			src: "src/template/sample/name.dust",
			dest: "dist/sample/name.html",
			options: {
				basePath: "./src/",
				context: [
					"src/template/data/sample/name.json"
				],
				whitespace: true,
				module: "dust-utilities-risui"
			}
		}
	});

	grunt.loadNpmTasks("grunt-dust-html");
};
