maven-archetype-dust-template
===========

In order to run the web app content:

1) Run the R:/DevTools/Install-Web-NoIDE script to install Nodejs, Ruby, Sass, etc...
2) Run - npm install
	 to get correct node packages
3) Run -  grunt nodedev
	 to start server
4) Open the dist folder to open the html pages or use the url: 
http://localhost:8080/dist/index.html 
http://localhost:8080/dist/sample/name.html 


To run it on websphere/prepare for deployment
2) Run grunt javadev to copy the content to the WebContent folder.  Make sure to check-in this code and only edit web content within the FEdev folder.
