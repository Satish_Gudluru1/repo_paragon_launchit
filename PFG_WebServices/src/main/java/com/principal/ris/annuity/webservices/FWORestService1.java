package com.principal.ris.annuity.webservices;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.principal.ris.annuity.communication.domain.Communication;
import com.principal.ris.annuity.contract.domain.Contract;
import com.principal.ris.annuity.disbursement.domain.Disbursement;
import com.principal.ris.annuity.tax.domain.Tax;

@RestController
//@RequestMapping(value="/fwo")
public class FWORestService1 {
	
	@RequestMapping("/fwo/contractinfo11/{contractId}")
	public Contract getContractInfo(@PathVariable String contractId){
		
		
		ObjectMapper mapper = new ObjectMapper();
		Contract obj = null;
		//JSON from file to Object
		try {
//			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
//			InputStream input = classLoader.getResourceAsStream("rest_endpoints.properties");		
			obj = mapper.readValue(new File("6100550 Contract JSON.json"), Contract.class);
		} catch (JsonParseException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		
		return obj;
	}
	
	@RequestMapping("/customerinfo")
	public Communication getCustomerInfo(){
		
		return null;
	}
	
	@RequestMapping("/taxinfo")
	public Tax getTaxInfo(){
		
		return null;
	}
	
	@RequestMapping("/deliverymethodinfo")
	public Disbursement getDMInfo(){
		
		return null;
	}

}
