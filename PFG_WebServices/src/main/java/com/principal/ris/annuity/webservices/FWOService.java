package com.principal.ris.annuity.webservices;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.principal.ris.annuity.bank.domain.Bank;
import com.principal.ris.annuity.communication.domain.Communication;
import com.principal.ris.annuity.contract.domain.Contract;
import com.principal.ris.annuity.disbursement.domain.Disbursement;
import com.principal.ris.annuity.product.domain.Product;
import com.principal.ris.annuity.role.domain.ContractRole;
import com.principal.ris.annuity.tax.domain.Tax;

@Service
public class FWOService {
	
	ObjectMapper mapper;
	
    public FWOService(){
    	System.out.println("In fwo service...contructor..............................");
	    mapper = new ObjectMapper();
		  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		  mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE, false);
    }
    
    public Contract getContract(String contractId){
    			//String basePath = request.getServletContext().getRealPath("/");
    			//File file = new File(basePath+"assets/6100550 Contract JSON.json");
    			//System.out.println(basePath);
    			Contract obj = null;
    		
    			try {
    					
    				obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\testRepo\\6100550 PSSA FWO\\6100550 Contract JSON.json"), Contract.class);
    				//obj = mapper.readValue(file, Contract.class);
    			} catch (JsonParseException e) {			
    				e.printStackTrace();
    			} catch (JsonMappingException e) {			
    				e.printStackTrace();
    			} catch (IOException e) {			
    				e.printStackTrace();
    			}
    			System.out.println(contractId);
    			if(obj.getContractId().trim().equals(contractId))
    		 
    			 return obj;
    			else 
    				return new Contract();
	}
    
    public Bank getBank(String id){
		Bank obj = null;
		try {
				obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\testRepo\\6100550 PSSA FWO\\6100550 Bank Json.json"), Bank.class);
		} catch (JsonParseException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		return obj;
	}
    
    public Communication getCommunication(String id){
		Communication obj = null;
		try {
				obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\testRepo\\6100550 PSSA FWO\\6100550 Communication JSON.json"), Communication.class);
		} catch (JsonParseException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		if(obj.getContractId().equals(id))
		     return obj;
		else 
			return new Communication();
	}
    
    public Product getProduct(String id){
		Product obj = null;
		try {
			   obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\testRepo\\6100550 PSSA FWO\\6100550 Product JSON.json"), Product.class);
		} catch (JsonParseException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		return obj;
	}
    
    public Disbursement getDisbusrsement(String id){
		Disbursement obj = null;
		try {
			obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\testRepo\\6100550 PSSA FWO\\6100550 disbursment json.json"), Disbursement.class);
		} catch (JsonParseException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		if(obj.getContractId().equals(id))
		    return obj;
		else 
			return new Disbursement();
	}
    public Tax getTaxInfo(String id){
		Tax obj = null;
		try {
			obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\testRepo\\6100550 PSSA FWO\\6100550 Tax Json.json"), Tax.class);
		} catch (JsonParseException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		if(obj.getContractId().equals(id))
		    return obj;
		else 
			return new Tax();
	}
    
    public ContractRole getRole(String id){
    	ContractRole obj=null;
		try {
			obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\testRepo\\6100550 PSSA FWO\\6100550 role json.json"), ContractRole.class);
		} catch (JsonParseException e) {
				e.printStackTrace();
		} catch (JsonMappingException e) {
				e.printStackTrace();
		} catch (IOException e) {
					e.printStackTrace();
			}
		    return obj;
	}      
    
}
