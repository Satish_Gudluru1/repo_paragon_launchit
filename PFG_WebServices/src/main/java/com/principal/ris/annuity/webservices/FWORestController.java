package com.principal.ris.annuity.webservices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.principal.ris.annuity.bank.domain.Bank;
import com.principal.ris.annuity.communication.domain.Communication;
import com.principal.ris.annuity.contract.domain.Contract;
import com.principal.ris.annuity.disbursement.domain.Disbursement;
import com.principal.ris.annuity.product.domain.Product;
import com.principal.ris.annuity.role.domain.ContractRole;
import com.principal.ris.annuity.tax.domain.Tax;

@RestController
public class FWORestController {
	
	
	@Autowired
	FWOService service;
	
	@RequestMapping("/contractinfo/{contractId}")
	public Contract getContractInfo(@PathVariable String contractId,HttpServletRequest request,HttpServletResponse response){
       return service.getContract(contractId);		
	}
	
	@RequestMapping(value="/bankinfo/{id}",method=RequestMethod.GET)
	public Bank getBank(@PathVariable String id){
		return service.getBank(id);
	}
	
	@RequestMapping(value="/communication/{id}",method=RequestMethod.GET)
	public Communication getCommunication(@PathVariable String id){
		return service.getCommunication(id);
	}
	
	@RequestMapping(value="/product/{id}",method=RequestMethod.GET)
	public Product getProduct(@PathVariable String id){
		return service.getProduct(id);
	}
	
	// For Delivery Method and Financial transactions	
	@RequestMapping(value="/disbursment/{id}",method=RequestMethod.GET)
	public Disbursement getDisbusrsement(@PathVariable String id){
		return service.getDisbusrsement(id);
	}
	
	@RequestMapping(value="/taxinfo/{id}",method=RequestMethod.GET)
	public Tax getTax(@PathVariable String id){
		return null;
	}
	
	@RequestMapping(value="/contractrole/{id}",method=RequestMethod.GET)
	public ContractRole getRole(@PathVariable String id){
		return service.getRole(id);
	}
	
	
	
	
	
	/*

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    @RequestMapping("/fwo/contractinfo/{contractId}")
	public Contract getContractInfo(@PathVariable String contractId){
		
		
		ObjectMapper mapper = new ObjectMapper();
//		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
//		InputStream input = classLoader.getResourceAsStream("6100550 Contract JSON.json");	
		Contract obj = null;
		//JSON from file to Object
		try {
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			obj = mapper.readValue(new File("C:\\Work_Satish\\Principal_Financials\\Webservice_Example\\gs-rest-service-master\\complete\\src\\main\\resources\\6100550_Contract_JSON.json"), Contract.class);
		} catch (JsonParseException e) {			
			e.printStackTrace();
		} catch (JsonMappingException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		
		return obj;
	}
*/
	

}
