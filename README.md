App
=================
Reference implementation of a Spring MVC backed Dust app using Femur, Spring Boot, and Horizon

Build and Run
=============

In order to run the web app content on websphere (see the FEdev README instruction for running on Node):
1) Install Nodejs, Ruby, Sass
2) Run - npm install to get correct node packages
3) Run  - grunt javadev 
     to copy the content to the WebContent folder.  
4) Make sure to check-in this code and only edit web elements within the FEdev folder not directly in the WebContent folder.
5) Hit the URL locally here:
http://inside09.local.theprincipal.net:9080/dust/index.html
http://inside09.local.theprincipal.net:9080/dust/sample/name.html

JavaDoc Standards
===============
I wanted to mention that we have Javadoc inside this applicaiton on various methods.  For our Applications, we typically do not require javadoc on methods unless they are unusual or have some extra complexity to them.  I included what is currently in the application to help assist someone who is getting started.  They can actually be removed once the user feels comfortable working with this app.  Regular comments are encouraged.


##test - satish
